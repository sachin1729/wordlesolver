//@ts-check

//Example of input json: {"a":3,"b":[-1,-3,-4],"e":0}
//This implies that a is at 3rd position, b is not at 1st,3rd and 4th, e is not present at all
// In input json all indices start from 1.
// 0 ---> character in not present
// list of -ve positions ---> character in present, but not on the index === mod(the -ve number)
// +ve ---> character is present at the index


const readline = require('readline');
const fs = require('fs');
//const wordListFile = 'filteredWords.txt'
const wordListFile = 'filteredWordsFreqSorted.txt'
const suggestionsFile = 'suggestedWords.txt'
let wordsList = []

function chooseRandomWord() {
    return wordsList[Math.floor(Math.random()*30)];
}

async function createWordList() {
    return new Promise((resolve, reject) => {
        var lineReader = readline.createInterface({
            input: fs.createReadStream(wordListFile)
        });
        
        lineReader.on('line', line => {
            wordsList.push(line)
        });

        lineReader.on('error', (error) => {
            reject(error);
        })
        
        lineReader.on('close', function () {
            wordsList = wordsList.filter(x=>x)
            console.log(`\n---------Word list(${wordsList.length} words) created--------\n`)
            resolve();
        })
    })
}

const nextQuestion = (input) => {
    return new Promise((resolve, reject) => {
        input.question(`What's the input json? e.g. {"a":3,"b":[-1,-3,-4],"e":0} \n`, (stringifiedJson) => {
            if(!stringifiedJson) {
                console.log('Try: ', chooseRandomWord())
                return resolve();
            }
            let infoJson
            try{
                infoJson = JSON.parse(stringifiedJson);
            } catch (error) {
                console.log('Please enter a valid json. Maybe use double quotes.');
                return reject(error);
            }
            suggestWords(infoJson);
            console.log("Suggested words have been written to file: ", suggestionsFile);
            return resolve();
        })
    })
}

function suggestWords(infoJson) {
    //console.log("INFOJSON:: ", infoJson)
    fs.writeFileSync(suggestionsFile, '');
    let noNoLetters = Array.from(new Set(Object.keys(infoJson).filter(char=>infoJson[char]===0)));
    let wrongPositionalLettersJson = {};
    let correctPositionalLettersJson = {};
    Object.keys(infoJson).forEach(char=>{
        if(infoJson[char].constructor == Array) {
            let positions = infoJson[char].filter(x=>x);
            if(positions.length){
                positions.forEach(pos=>{
                    if(pos > 0 && pos <= 5) {
                        correctPositionalLettersJson[char] = pos-1;
                    } else if(pos < 0 && pos >= -5) {
                        wrongPositionalLettersJson[char] = wrongPositionalLettersJson[char] || [];
                        wrongPositionalLettersJson[char].push((-1)*pos-1);
                    }
                })
            }
        } else if (infoJson[char].constructor == Number) {
            if(infoJson[char] > 0 && infoJson[char] <= 5) {
                correctPositionalLettersJson[char] = infoJson[char]-1;
            } else if(infoJson[char] < 0 && infoJson[char] >= -5) {
                wrongPositionalLettersJson[char] = [(-1)*infoJson[char]-1];
            }
        }
    });

    //console.log("PARSED:: ", noNoLetters, wrongPositionalLettersJson, correctPositionalLettersJson);
    let suggestedWords = wordsList.filter(word=>!noNoLetters.some(letter=>word.includes(letter)));
    suggestedWords = suggestedWords.filter(word=>{
        return !Object.keys(correctPositionalLettersJson).some(char=>word.charAt(correctPositionalLettersJson[char])!==char)
    })
    suggestedWords = suggestedWords.filter(word=>{
        return !Object.keys(wrongPositionalLettersJson).some(char=>!word.includes(char)) &&
        !Object.keys(wrongPositionalLettersJson).some(char=>{
            return wrongPositionalLettersJson[char].some(pos=>word.charAt(pos)===char)
            //word.charAt(wrongPositionalLettersJson[char])===char
        })
    })
    suggestedWords.forEach(word=>{
        fs.appendFileSync(suggestionsFile, word+'\n');
    })

}

async function main() {
    try {
        await createWordList();
        const input = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        let inputSuccess = false;
        do {
            await nextQuestion(input).then(x=>{inputSuccess = true}).catch(x=>x);
        } while(!inputSuccess)

        input.close()

    } catch(error) {
        console.log('ERROR:: ', error);
    }
}


main();

