//@ts-check

const fs = require('fs');
const readline = require('readline');

let inputFile = 'unigram_freq.txt'
let processedFile = 'filteredWordsFreqSorted.txt'
let wordsFreqMap = {}
fs.writeFileSync(processedFile, '');

var lineReader = readline.createInterface({
    input: fs.createReadStream(inputFile)
  });
  
lineReader.on('line', function (line) {
    let word = line.split(',')[0]
    let freq = Number(line.split(',')[1])
    //console.log("line", line)
    //console.log("WORD:: ", word, freq)
    if(word.length === 5) {
        wordsFreqMap[word] = freq
        //fs.appendFileSync(processedFile, line+'\n');
    }
    //console.log('Line from file:', line);
});


setTimeout(()=>{
    let wordsList = Object.keys(wordsFreqMap);
    wordsList.sort((x, y)=>(wordsFreqMap[x]>wordsFreqMap[y])?-1:1);
    wordsList.forEach(word=>{
        fs.appendFileSync(processedFile, word+'\n');
    })
    console.log("Done")
}, 10000)


